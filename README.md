# Gonashop


## About

An open-source e-commerce store.


## License

Gonashop is an open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
